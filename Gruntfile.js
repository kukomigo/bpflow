module.exports = function (grunt) {
  'use strict';

  // Load all npm dependencies
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),


    //
    // Configure projects validators
    // -----------------------------------------

    coffee_jshint: {
      source: {
        options: {
          globals: ["console", "alert", "$"]
        },
        src: ['src/**/*.coffee']
      }
    },

    lesslint: {
      source: {
        options: {
          csslint: {
            "known-properties": false
          }
        },
        src: ['src/styles/*.less']
      }
    },


    //
    // Configure watching project's development
    // -----------------------------------------

    express: {
      development: {
        options: {
          port: 9000,
          hostname: 'localhost',
          bases: ['app'],
          livereload: true
        }
      }
    },

    open: {
      development: {
        url: 'http://localhost:<%= express.development.options.port %>'
      }
    },

    watch: {
      coffee: {
        files: 'src/**/*.coffee',
        tasks: ['coffee_jshint', 'coffee']
      },
      less: {
        files: 'src/styles/*.less',
        tasks: ['lesslint', 'less']
      },
      html: {
        files: 'src/**/*.html',
        tasks: ['copy:development']
      },
      app: {
        files: ['app/**/*.js', 'app/styles/*.css', 'app/**/*.html'],
        options: {
          livereload: true
        }
      }
    },


    //
    // Configure installing project's files
    // -----------------------------------------

    bower: {
      development: {
        options: {
          targetDir: 'app/bower',
          cleanBowerDir: true
        }
      }
    },

    clean: {
      development: ['app'],
      production: ['dist']
    },


    //
    // Configure project's development flow
    // -----------------------------------------

    coffee: {
      development: {
        expand: true,
        flatten: false,
        cwd: 'src',
        src: ['**/*.coffee'],
        dest: 'app',
        ext: '.js'
      }
    },

    less: {
      development: {
        options: {
          paths: ['src/styles']
        },
        files: {
          'app/styles/application.css': 'src/styles/application.less'
        }
      }
    },

    copy: {
      development: {
        files: [
          {expand: true, flatten: false, cwd: 'src', src: ['**/*.html'], dest: 'app'}
        ]
      }
    },

    karma: {
      development: {
        options: {
          files: ['app/**/*.js' ,'test/**/*.js'],
          singleRun: true,
          frameworks: ['jasmine'],
          browsers: ['PhantomJS']
        }
      }
    }

    //
    // Configure building project's files
    // -----------------------------------------

  });


  //
  // Register all important tasks
  // -----------------------------------------

  // I'm pretty out of habit to use this
  grunt.registerTask('lint', ['coffee_jshint', 'lesslint']);

  // Compile less and coffee scripts
  grunt.registerTask('prepare', ['clean:development' ,'bower', 'copy:development' ,'coffee', 'less']);

  // Open server and listen to all changes
  grunt.registerTask('run', ['lint' ,'prepare', 'express', 'open', 'watch']);

  // Run tests
  grunt.registerTask('test', ['lint' ,'prepare', 'karma']);

};
