# JavaScript boilerplate - perfect flow

Build your projects clean and neat.

## Flow

My primary intension is to create a "perfect" flow for javascript developer. Basic idea is to divide project to a 3 areas (src, app, dist). 

1. First area (src) contains only your code, its not poluted by any of 3rd party vendors nor compiled files.
2. Second area (app) is dynamicaly created and contains compiled code (coffee, less, sass, etc.).
3. Third, last area (dist) contains compiled and minified code ready to release.

## Setup

First make sure you have installed node.js >= 0.10 and grunt-cli.
Clone this repo to your folder, add to bower.json any library you need.
Hit ```npm install``` to install all dev dependencies for grunt.
Great, that's it! Now you're ready to develop!

## Tasks

Thanks to grunt and many plugins we can buid our projects fater.

Check you coffee and less files:
```
$ grunt lint
```

Compile all files:
```
$ grunt prepare
```

Run development flow:
```
$ grunt run
```

Run your tests:
Check you coffee and less files:
```
$ grunt test
```

## TODO

Building process! Minify and concat files.
