fill = (container, liquid = "coffee") ->
  console.log "Filling the #{container} with #{liquid}..."

fill "cup"

square = (x) -> x * x
cube   = (x) -> square(x) * x